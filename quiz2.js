//start play function
function play(){
	gamePieces=["rock", "paper", "sissors", "dynamite"];
	var userPiece=userSelection();
	//alert(userPiece); //debugging check
	var gamePiece=getRandomGamePiece(gamePieces);
	//alert(gamePiece); //debugging check
	//alert("user = "+userPiece+" computer = "+gamePiece) //debugging check
	var winner=whoWins(userPiece, gamePiece)
	//alert("the winner is "+winner) //debugging check

	//print the winner to div id=results
	$(function(){
		//alert(winner) //debugging check
		if(winner=="user"){
			$("#results").html("<p>Congrats "+winner+" wins!</p><p>You had "+userPiece+" and the Computer choose "+gamePiece+".</p>");
		}else if(winner=="computer"){
			$("#results").html("<p>Sorry but "+winner+" wins :(</p><p>You had "+userPiece+" and the Computer choose "+gamePiece+".</p>");
		}else{
			$("#results").html("<p>"+winner+" try again!</p>");
		}
	});
};
//get and validate the user selection
function userSelection(){
	userPiece=prompt("Do you Choose Rock, Paper, Sissors or Dynamite");
	userPiece=userPiece.toLowerCase();
	if(userPiece!="rock"&&userPiece!="paper"&&userPiece!="sissors"&&userPiece!="dynamite"){
		alert("Please select one of Rock, Paper, Sissors or Dynamite");
		userSelection();
	};
	return userPiece
};
//get the computers gamePiece
function getRandomGamePiece(gamePieces){
	rndNum=Math.floor(Math.random()*gamePieces.length)
	//alert("my random num = "+rndNum) //debugging check
	switch(rndNum){
		case 0:
			gamePiece=gamePieces[0];
			break;
		case 1:
			gamePiece=gamePieces[1];
			break;
		case 2:
			gamePiece=gamePieces[2];
			break;
		case 3:
			gamePiece=gamePieces[3];
			break;
	};
	//alert("gamePiece = "+gamePiece) //debugging check
	return gamePiece
};
//decide who wins
function whoWins(user, computer){
	var winner;
	//alert("user = "+user+" computer = "+computer) //debugging check
	if(user=="rock"){
		if(computer=="sissors"){
			winner="user";
		}else if(computer=="rock"){
			winner="You both choose Rock"
		}else{
			winner="computer";
		};
	}else if(user=="paper"){
		if(computer=="rock"){
			winner="user";
		}else if(computer=="paper"){
			winner="You both choose Paper"
		}else{
			winner="computer";
		};
	}else if(user=="sissors"){
		if(computer=="paper"){
			winner="user";
		}else if(computer=="dynamite"){
			winner="user"
		}else if(computer=="sissors"){
			winner="You both choose Sissors"
		}else{
			winner="computer";
		};
	}else if(user=="dynamite"){
		if(computer=="rock"){
			winner="user";
		}else if(computer=="paper"){
			winner="user"
		}else if(computer=="dynamite"){
			winner="You both choose Dynamite"
		}else{
			winner="computer";
		};
	};
	//alert(winner) //debugging check
	return winner;
};