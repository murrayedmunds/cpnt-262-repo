
function loadProvinces(){
	//create and populate proArray
	var provArray = ["British Columbia", "Alberta", "Saskatchewan", "Manitoba", "Ontario", "Quebec", "Prince Edward Island", "Nova Scotia", "New Brunswick", "Newfoundland & Labrador"]
	//start select combo box
    document.write('<select name="cboProv" id="cboProv">');
    document.write('<option name="cbo1" id="cbo1" value="">- select-</option>');
    //use for loop to populate options in cboProv
    for(i=0; i<provArray.length; i++){
    	document.write('<option name="cbo' + (i+2) + '" value="' + provArray[i] + '">' + provArray[i] + '</option>');
    }
    document.write('</select><br>');
    //create name input box
    document.write('<br><label>Name </label>');
    document.write('<input type="text" name="txtName" id="txtName"><br>');
    //create email input box
    document.write('<br><label>Email </label>');
    document.write('<input type="text" name="txtEmail" id="txtEmail"><br>');
    //create submit button
    document.write('<br><button name="submit" onclick="validateCheck()">Submit</button>');
}

function validateCheck(){
	/*check that a option has been selected for cboProv */
	if(document.getElementById('cboProv').selectedIndex == 0){
		alert('Please Select a Province');
		document.getElementById('cboProv').focus();
	}
	//check that a name has been entered
	else if(document.getElementById('txtName').value == ''){
		alert('Please Enter a Name');
		document.getElementById('txtName').focus();
	}
	//check that a email has been entered
	else if(document.getElementById('txtEmail').value == ''){
		alert('Please Enter an Email Address');
		document.getElementById('txtEmail').focus();
	}
}